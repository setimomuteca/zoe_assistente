﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.Speech.Recognition;

namespace Zoe_App
{
    public partial class mainForm : Form
    {
        private SpeechRecognitionEngine engine;
        private CultureInfo ci;
        public string NOME_ASSISTENTE = "ZOE";
        
        public mainForm()
        {
            InitializeComponent();
            Init();
        }


        private void Init()
        {
            try
            {
                ci = new CultureInfo("pt_BR");
                engine = new SpeechRecognitionEngine(ci);

                SpeechRec();
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message, "Erro em Init()");
            }
        }

        private List<Grammar> LoadGrammar()
        {
            List<Grammar> gramaticasParaFala = new List<Grammar>();
            #region Choices
            Choices comandosSistema = new Choices();
            comandosSistema.Add(Gramaticas.PerguntaHoras.ToArray());
            #endregion

            #region GrammarBuilder
            GrammarBuilder comandosSistemaBuilder = new GrammarBuilder();
            comandosSistemaBuilder.Append(comandosSistema);
            comandosSistemaBuilder.Culture = new CultureInfo("pt_BR");
            #endregion

            #region Grammar
            Grammar gramaticasSistema = new Grammar(comandosSistemaBuilder);
            gramaticasSistema.Name = "system";
            
            #endregion

            gramaticasParaFala.Add(gramaticasSistema);

            return gramaticasParaFala;
        }



        private void SpeechRec()
        {
            try
            {

                List<Grammar> gramaticas = LoadGrammar();
                #region Reconhecimento
                engine.SetInputToDefaultAudioDevice();


                foreach (Grammar gr in gramaticas)
                {
                    engine.LoadGrammar(gr);
                }
                engine.SpeechRecognized += new EventHandler<SpeechRecognizedEventArgs>(Rec);
                engine.AudioLevelUpdated += new EventHandler<AudioLevelUpdatedEventArgs>(AudioLevel);
                engine.SpeechRecognitionRejected += new EventHandler<SpeechRecognitionRejectedEventArgs>(Rejected);
                #endregion


                engine.RecognizeAsync(RecognizeMode.Multiple); //Inicia o reconhecimento

            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message, "Erro em SpeechRec()");
            }
        }

        #region Metodos de Reconhecimentos
        private void Rec(object sender, SpeechRecognizedEventArgs e)//Reconhecido
        {
            lblStatus.Text = e.Result.Text;
            //Reconhecido
        }

        private void AudioLevel(object sender, AudioLevelUpdatedEventArgs e)
        {            
            if(e.AudioLevel > progressBar.Maximum)
            {
                progressBar.Value = progressBar.Maximum;
                return;
            }

            if (e.AudioLevel < progressBar.Minimum)
            {
                progressBar.Value = progressBar.Minimum;
                return;
            }
            progressBar.Value = e.AudioLevel;
            //Actualização dos dados 
        }


        private void Rejected(object sender, SpeechRecognitionRejectedEventArgs e)
        {
            
            lblStatus.Text = "Não reconhecido";
            //Rejeitado
        }

        #endregion










        private void mainForm_Load(object sender, EventArgs e)
        {

        }
    }
}
